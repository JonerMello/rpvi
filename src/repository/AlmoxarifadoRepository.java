package repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.AlmoxarifadoDAO;
import model.Almoxarifado;
import model.Produto;

public class AlmoxarifadoRepository {

	private static AlmoxarifadoRepository instance;
	private static List<Almoxarifado> listaAlmoxarifado;
	private static AlmoxarifadoDAO almoxarifadoDao;
	
	public AlmoxarifadoRepository() {
		almoxarifadoDao = new AlmoxarifadoDAO();
		listaAlmoxarifado = new ArrayList<Almoxarifado>();
	}
	
	public static synchronized AlmoxarifadoRepository getInstance() {
		if (instance == null) {
			instance = new AlmoxarifadoRepository();
		}
		return instance;
	}
	
	
	public void autenciarAlmoxarifado(String email, String senha) {
		
		ResultSet rs = almoxarifadoDao.select(email, senha);
		
		try {
			while(rs.next()) {
				
				int codigo = rs.getInt("cod_almoxarifado");
				String nome = rs.getString("nome_unidade");
				String tipo = rs.getString("tipo_unidade");
				String email_unidade = rs.getString("email");
				
				Almoxarifado almox = new Almoxarifado(codigo,nome,tipo,email_unidade);
				listaAlmoxarifado.add(almox);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	public static List<Almoxarifado> getListaAlmoxarifado() {
		return listaAlmoxarifado;
	}

	public static void setListaAlmoxarifado(List<Almoxarifado> listaAlmoxarifado) {
		AlmoxarifadoRepository.listaAlmoxarifado = listaAlmoxarifado;
	}
	
	
	
	
}
