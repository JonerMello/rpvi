package repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.ConsultaEstoqueDAO;
import dao.ProdutoDAO;
import model.Produto;

public class ConsultaEstoqueRepository {

	private static ConsultaEstoqueRepository instance;
	private static List<Produto> listaProduto;
	private ConsultaEstoqueDAO consultaEstoqueDao;
	
	public ConsultaEstoqueRepository() {
		listaProduto = new ArrayList<Produto>();
		consultaEstoqueDao = new ConsultaEstoqueDAO();
	}
	
	public static synchronized ConsultaEstoqueRepository getInstance() {
		if (instance == null) {
			instance = new ConsultaEstoqueRepository();
		}
		return instance;
	}
	
	/**
	 * M�todo para buscar todos os produtos
	 * @return
	 */
	public void buscarTodosProdutos() {
		getListaProduto().clear();
		
		ResultSet rs = consultaEstoqueDao.select();
		
		try {
			while(rs.next()) {
				int codigoProduto = rs.getInt("cod_produto");
				int codigoAlmoxarifado = rs.getInt("cod_almox_almoxarife");
				String nome = rs.getString("nome_produto");
				String tipo = rs.getString("tipo_produto");
				String descricao = rs.getString("descricao_produto");
				String unidade = rs.getString("nome_unidade");
				int quantidade = rs.getInt("quantidade");
				
				String codigo = codigoProduto + "" + codigoAlmoxarifado;
				
				Produto produto = new Produto(Integer.parseInt(codigo),nome,descricao,tipo,unidade,quantidade);
				listaProduto.add(produto);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	public static List<Produto> getListaProduto() {
		return listaProduto;
	}

	public static void setListaProduto(List<Produto> listaProduto) {
		ConsultaEstoqueRepository.listaProduto = listaProduto;
	}

}
