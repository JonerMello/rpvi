package repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.ProdutoDAO;
import model.Produto;

public class ProdutoRepository {

	private static ProdutoRepository instance;
	private static List<Produto> listaProduto;
	private static List<Produto> listaSolicitacaoItemProduto;
	private ProdutoDAO produtoDao;
	
	public ProdutoRepository() {
		listaProduto = new ArrayList<Produto>();
		listaSolicitacaoItemProduto = new ArrayList<Produto>();
		produtoDao = new ProdutoDAO();
	}

	public static synchronized ProdutoRepository getInstance() {
		if (instance == null) {
			instance = new ProdutoRepository();
		}
		return instance;
	}
	
	/**
	 * M�todo que busca produtos com o nome definido como filtro
	 * @param filtro
	 * @return
	 */
	public void buscarProdutoNome(String filtro) {
		getListaProduto().clear();
			
		ResultSet rs = produtoDao.select(filtro);
		
		try {
			while(rs.next()) {
				int codigoProduto = rs.getInt("cod_produto");
				int codigoAlmoxarifado = rs.getInt("cod_almox_almoxarife");
				String nome = rs.getString("nome_produto");
				String tipo = rs.getString("tipo_produto");
				String descricao = rs.getString("descricao_produto");
				String unidade = rs.getString("nome_unidade");
				int quantidade = rs.getInt("quantidade");
				
				String codigo = codigoProduto + "" + codigoAlmoxarifado;
				
				Produto produto = new Produto(codigoProduto, codigoAlmoxarifado,nome,descricao,tipo,unidade,quantidade);
				listaProduto.add(produto);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	
	
	
	
	
	
	
	public static List<Produto> getListaProduto() {
		return listaProduto;
	}

	public static void setListaProduto(List<Produto> listaProduto) {
		ProdutoRepository.listaProduto = listaProduto;
	}

	public static List<Produto> getListaSolicitacaoItemProduto() {
		return listaSolicitacaoItemProduto;
	}

	public static void setListaSolicitacaoItemProduto(List<Produto> listaSolicitacaoItemProduto) {
		ProdutoRepository.listaSolicitacaoItemProduto = listaSolicitacaoItemProduto;
	}
	
}
