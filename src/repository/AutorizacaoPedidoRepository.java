package repository;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.AutorizacaoPedidoDAO;
import model.AutorizacaoPedido;
import model.Produto;
import model.SolicitacaoItemProduto;

public class AutorizacaoPedidoRepository {

	private static AutorizacaoPedidoRepository instance;
	private static List<AutorizacaoPedido> listaPedido;
	private AutorizacaoPedidoDAO autorizaPedidoDao;

	public AutorizacaoPedidoRepository() {
		listaPedido = new ArrayList<AutorizacaoPedido>();
		autorizaPedidoDao = new AutorizacaoPedidoDAO();
	}

	public static synchronized AutorizacaoPedidoRepository getInstance() {
		if (instance == null) {
			instance = new AutorizacaoPedidoRepository();
		}
		return instance;
	}

	/**
	 * M�todo para buscar todos os pedidos
	 * 
	 * @return
	 */
	public void buscarTodosPedidos() {
		getListaPedido().clear();

		ResultSet rs = autorizaPedidoDao.select();

		try {
			while (rs.next()) {
				int codigoPedido = rs.getInt("cod_pedido");
				int codigoProduto = rs.getInt("cod_produto");
				Date data = rs.getDate("data_hora");
				String unidPertencente = rs.getString("");
				String unidDestinataria = rs.getString("");
				String nomeProduto = rs.getString("nome_produto");
				String tipoProduto = rs.getString("tipo_produto");
				String quatidade = rs.getString("quantidade");

				AutorizacaoPedido pedido = new AutorizacaoPedido(codigoPedido, codigoProduto, data,
						unidPertencente, unidDestinataria, nomeProduto, tipoProduto, quatidade);
				listaPedido.add(pedido);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	public static List<AutorizacaoPedido> getListaPedido() {
		return listaPedido;
	}

	public static void setListaProduto(List<AutorizacaoPedido> listaPedido) {
		AutorizacaoPedidoRepository.listaPedido = listaPedido;
	}
}
