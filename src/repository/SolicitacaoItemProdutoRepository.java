package repository;

import java.util.ArrayList;

import dao.SolicitacaoItemProdutoDAO;
import model.Produto;
import model.SolicitacaoItemProduto;

public class SolicitacaoItemProdutoRepository {

	private static SolicitacaoItemProdutoRepository instance;
	private SolicitacaoItemProdutoDAO solicitacaoItemProdutoDao;
	
	public SolicitacaoItemProdutoRepository() {
		solicitacaoItemProdutoDao = new SolicitacaoItemProdutoDAO();
	}
	
	public static synchronized SolicitacaoItemProdutoRepository getInstance() {
		if (instance == null) {
			instance = new SolicitacaoItemProdutoRepository();
		}
		return instance;
	}
	
	public void enviarSolicitacaoItemProduto(SolicitacaoItemProduto solicitacao) {
	
		solicitacaoItemProdutoDao.create(solicitacao);
		
	}
}
