package controller;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import model.Almoxarifado;
import model.SessionContext;
import repository.AlmoxarifadoRepository;

@ManagedBean
public class AutenticacaoBean  {

	private String email;
	private String senha;
	private SessionContext session;
	private Almoxarifado almoxarifado;
	private boolean isLogado = false;
	
	public AutenticacaoBean(){
		session = new SessionContext();
		almoxarifado = new Almoxarifado();
	}
	
	public String autenticarAlmoxarifado(){
		
		if(AlmoxarifadoRepository.getInstance().getListaAlmoxarifado().size() > 0 && AlmoxarifadoRepository.getInstance().getListaAlmoxarifado().get(0).getTipo()!=null) {
			System.out.println("Usu�rio logado");
			 return "/index.xhtml?faces-redirect=true";
		}
		else{
			System.out.println("Entrou No nAO LOGADO");
			AlmoxarifadoRepository.getInstance().autenciarAlmoxarifado(email, senha);
			
			if(AlmoxarifadoRepository.getInstance().getListaAlmoxarifado().size() > 0) {
				almoxarifado = AlmoxarifadoRepository.getInstance().getListaAlmoxarifado().get(0);	
			}		
			if(almoxarifado.getTipo() == null) {
				
				AlmoxarifadoRepository.getInstance().getListaAlmoxarifado().clear();
				return "/index.xhtml?faces-redirect=true";
			
			}else if(almoxarifado.getTipo().equals("Central")) {
				getSession().setAttribute("user", AlmoxarifadoRepository.getInstance().getListaAlmoxarifado().get(0));
				System.out.println("CENTRAL");
				setLogado(true);
				
				return "indexAlmoxarifadoCentral.xhtml";
				
			}else if(almoxarifado.getTipo().equals("Local")){
				getSession().setAttribute("user", AlmoxarifadoRepository.getInstance().getListaAlmoxarifado().get(0));
				System.out.println("LOCAL");
				setLogado(true);
				return "indexAlmoxarifadoLocal.xhtml";
			}
		}
		
		return "";
		
	}
	
	
	public String deslogarUsuario() {
		 getSession().encerrarSessao();
	       
	        mensagem("Voc� encerrou sua sess�o com sucesso!", "");
	        return "index.xhtml";
	}
	
	public String exibirUsuario() {
		Almoxarifado almox = (Almoxarifado) getSession().getAttribute("user");
		return "Unidade " + almox.getUnidade();
	}
	
	
	  public void error() {
	        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro!", "Usu�rio ou senha incorretos."));
	    }

	    public void mensagem(String mensagem, String detalhe) {
	        FacesContext context = FacesContext.getCurrentInstance();

	        context.addMessage(null, new FacesMessage(mensagem, detalhe));
	    }


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public SessionContext getSession() {
		return session;
	}

	public void setSession(SessionContext session) {
		this.session = session;
	}

	public Almoxarifado getAlmoxarifado() {
		return almoxarifado;
	}

	public void setAlmoxarifado(Almoxarifado almoxarifado) {
		this.almoxarifado = almoxarifado;
	}

	public boolean isLogado() {
		if(getSession().getAttribute("user")!=null) {
			return true;
		}
		
		return false;
	}

	public void setLogado(boolean isLogado) {
		this.isLogado = isLogado;
	}
		
	
	
}
