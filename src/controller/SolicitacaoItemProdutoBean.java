package controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

import dao.ProdutoDAO;
import dao.SolicitacaoItemProdutoDAO;
import model.Produto;
import model.SessionContext;
import model.SolicitacaoItemProduto;
import repository.ProdutoRepository;
import repository.SolicitacaoItemProdutoRepository;

@ManagedBean (name = "produtoBean")
@ViewScoped
public class SolicitacaoItemProdutoBean implements Serializable {
	
	private int codigo;
	private String nome;
	private String descricao;
	private String tipo;
	private String unidade;
	private String observacao;
	
	private List<Produto> listaBuscaProduto;
	private List<Produto> listaSolicitacao;
	
	private Produto produto;
	private Produto produtoRemove;
	private ProdutoDAO produtoDao;
	private SolicitacaoItemProdutoDAO solicitacaoItemProdutoDao;
	
	private SessionContext session;
	
	public SolicitacaoItemProdutoBean() {
		
		listaBuscaProduto = new ArrayList<Produto>();
		listaSolicitacao = new ArrayList<Produto>();
		 
		produto = new Produto();
		produtoRemove = new Produto();
		 
		produtoDao = new ProdutoDAO(); 
		solicitacaoItemProdutoDao = new SolicitacaoItemProdutoDAO();
		
		session = new SessionContext();
	}
	
	/**
	 * M�todo que busca os produtos de acordo com a String de busca do usu�rio
	 * @param filtro
	 */
	public void buscarProduto(String filtro) {
		listaBuscaProduto.clear();
		if(filtro.length() > 0) {
			ProdutoRepository.getInstance().buscarProdutoNome(filtro);
			listaBuscaProduto = ProdutoRepository.getInstance().getListaProduto();
		}
		
	}
	
	/**
	 * M�todo que adiciona produtos ao pedido de solicita��o de itens de produto
	 * @param codigo
	 */
	public void adicionarProdutoSolicitacao(int codigo) {
		for(int i = 0; i < listaBuscaProduto.size(); i++) {
			if(listaBuscaProduto.get(i).getCodigo() == codigo) {
				if(!listaSolicitacao.contains(listaBuscaProduto.get(i))) {
					listaSolicitacao.add(listaBuscaProduto.get(i));
				}else {
					
					 RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
				}
			}
		}
	}
	
	/**
	 * M�todo que remove produtos do pedido de solicita��o de itens de produto
	 * @param codigo
	 */
	public void removerProdutoPedidoCompras(int codigo) {
		for(int i = 0; i < listaSolicitacao.size(); i++){
			if(listaSolicitacao.get(i).getCodigo() == codigo) {
				listaSolicitacao.remove(listaSolicitacao.get(i));
			}
		}
	}
		
	/**
	 * Evento que define o produto selecionado pelo usu�rio na tabela de busca
	 * @param event
	 */
	 public void onRowSelect(SelectEvent event) {       
		 produto = (Produto) event.getObject();      
	 }
	 
	 /**
		 * Evento que define o produto selecionado pelo usu�rio na tabela de solicita��o de itens de produto
		 * @param event
		 */
	public void onRowSelectRemove(SelectEvent event) {       
		produtoRemove = (Produto) event.getObject();      
	}
	 
	 /**
	  * Evento que chama o m�todo que adiciona produtos no pedido de solicita��o de itens de produto
	  */
	 public void eventoAdicionarProduto() {
		 adicionarProdutoSolicitacao(produto.getCodigo());
	 }
	 
	 /**
	  * Evento que chama o m�todo que remove produtos no pedido de solicita��o de itens de produto
	  */
	 public void eventoRemoverProduto() {
		 removerProdutoPedidoCompras(produtoRemove.getCodigo());
	 }
	 
	 /**
	  * 
	  */
	 public void enviarPedidoSolicitacaoProduto() {
		 
		 for(int i = 0; i < listaSolicitacao.size(); i ++) {
			 Date data = new Date();
			 System.out.println(data);
			 System.out.println(listaSolicitacao.get(i).getQuantidade());
			 SolicitacaoItemProduto solicitacao = new SolicitacaoItemProduto(data, listaSolicitacao.get(i).getQuantidade(), "Observacao", listaSolicitacao.get(i));
			 SolicitacaoItemProdutoRepository.getInstance().enviarSolicitacaoItemProduto(solicitacao);
		 }
		 
		
	 }
	  
	 public List<Produto> getListaBuscaProduto() {
			return listaBuscaProduto;
		}

	 public void setListaFiltro(List<Produto> listaBuscaProduto) {
		this.listaBuscaProduto = listaBuscaProduto;
	 }

	 public int getCodigo() {
		return codigo;
	 }

		public void setCodigo(int codigo) {
			this.codigo = codigo;
		}

		public String getNome() {
			return nome;
		}

		public void setNome(String nome) {
			this.nome = nome;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		public String getTipo() {
			return tipo;
		}

		public void setTipo(String tipo) {
			this.tipo = tipo;
		}

		public String getUnidade() {
			return unidade;
		}

		public void setUnidade(String unidade) {
			this.unidade = unidade;
		}

		public String getObservacao() {
			return observacao;
		}

		public void setObservacao(String observacao) {
			this.observacao = observacao;
		}

		public List<Produto> getListaSolicitacao() {
			return listaSolicitacao;
		}


		public void setListaSolicitacao(List<Produto> listaSolicitacao) {
			this.listaSolicitacao = listaSolicitacao;
		}
}
