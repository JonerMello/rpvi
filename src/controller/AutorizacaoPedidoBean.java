package controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import model.AutorizacaoPedido;
import model.Produto;
import repository.AutorizacaoPedidoRepository;
import repository.ConsultaEstoqueRepository;

@ManagedBean (name = "autorizacaoBean")
@ViewScoped
public class AutorizacaoPedidoBean {

	private String observacao;

	private AutorizacaoPedido pedido;
	
	private List<AutorizacaoPedido> listaSolicitacao;
	
	@PostConstruct
	public void init() {
		buscarSolicitacoes();
	}

	/**
	 * M�todo que procura os pedidos realizados
	 * 
	 */
	public void buscarSolicitacoes() {
		AutorizacaoPedidoRepository.getInstance().buscarTodosPedidos();
		listaSolicitacao = AutorizacaoPedidoRepository.getInstance().getListaPedido();

	}

	/**
	 * M�todo que autoriza os pedidos
	 */
	public void autorizarPedido(String obs) {

	}

	/**
	 * M�todo que nega os pedidos
	 */
	public void negarPedido(String obs) {
		
	}


	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public List<AutorizacaoPedido> getListaSolicitacao() {
		return listaSolicitacao;
	}

	public void setListaSolicitacao(List<AutorizacaoPedido> listaSolicitacao) {
		this.listaSolicitacao = listaSolicitacao;
	}
}
