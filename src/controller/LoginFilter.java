package controller;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Almoxarifado;
import model.SessionContext;

public class LoginFilter implements Filter {

	public void destroy() {
           // TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		Almoxarifado almox = null;
		HttpSession sess = ((HttpServletRequest) request).getSession(false);
        
        if (sess != null){
              almox = (Almoxarifado) sess.getAttribute("user");
        }      

              if (almox == null) {
            
                       String contextPath = ((HttpServletRequest) request)
                                          .getContextPath();
                       ((HttpServletResponse) response).sendRedirect(contextPath
                                          + "/index.xhtml");
              } else {
                       chain.doFilter(request, response);
              }

    }

    public void init(FilterConfig arg0) throws ServletException {
              // TODO Auto-generated method stub

    }
		

	
}
