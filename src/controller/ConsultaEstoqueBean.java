package controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import dao.ConsultaEstoqueDAO;
import dao.ProdutoDAO;
import model.Produto;
import repository.ConsultaEstoqueRepository;
import repository.ProdutoRepository;

@ManagedBean (name = "consultaBean")
@ViewScoped
public class ConsultaEstoqueBean {

	private int codigo;
	private String nome;
	private String descricao;
	private String tipo;
	private String unidade;


	private List<Produto> listaBuscaProduto;

	private Produto produto;
	private ConsultaEstoqueDAO consultaDao;
	
	@PostConstruct
	public void init() {
		buscarTodosProdutos();
	}
	
	/**
	 * M�todo que busca todos os produtos do estoque
	 */
	public void buscarTodosProdutos() {
		ConsultaEstoqueRepository.getInstance().buscarTodosProdutos();
		listaBuscaProduto = ConsultaEstoqueRepository.getInstance().getListaProduto();
	}

	public List<Produto> getListaBuscaProduto() {
		return listaBuscaProduto;
	}

	public void setListaFiltro(List<Produto> listaBuscaProduto) {
		this.listaBuscaProduto = listaBuscaProduto;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}
}
