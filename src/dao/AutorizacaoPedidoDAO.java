package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AutorizacaoPedidoDAO {

	private Connection con;

	public AutorizacaoPedidoDAO() {
		try {
			con = ConnectionFactory.getConnection();
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	public void update() {
		String query = "";

		try {

			PreparedStatement ps = con.prepareStatement(query);
			ps.execute();
			// con.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	public ResultSet select() {

		String query = "SELECT \"Pedido\".cod_produto, cod_pedido, nome_produto, tipo_produto, unidade_pertencente, unidade_destinataria, data_hora, quantidade "
				+ "FROM \"Pedido\" INNER JOIN \"Produto\" ON \"Pedido\".cod_produto = \"Produto\".cod_produto ";

		PreparedStatement ps;
		ResultSet rs = null;

		try {
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return rs;
	}
}
