package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AlmoxarifadoDAO {
	
	private Connection con;
	
	public AlmoxarifadoDAO() {
		try {
			con = ConnectionFactory.getConnection();
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	public ResultSet select(String email, String senha) {
		
		String query = "SELECT cod_almoxarifado, nome_unidade, tipo_unidade, email FROM \"Almoxarifado_Almoxarife\" "
				+ "WHERE \"Almoxarifado_Almoxarife\".email = '" + email + "' AND "
						+ "\"Almoxarifado_Almoxarife\".senha = '" + senha + "'";
		
		PreparedStatement ps;
		ResultSet rs = null;
		
		try {
			
			ps = con.prepareStatement(query);
			
			rs = ps.executeQuery();
				
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
			return rs;
	}
}
