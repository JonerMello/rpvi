package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConsultaEstoqueDAO {

	private Connection con;

	public ConsultaEstoqueDAO() {
		try {
			con = ConnectionFactory.getConnection();
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	public ResultSet select() {

		String query = "SELECT \"Estoque\".cod_produto, \"Estoque\".cod_almox_almoxarife, nome_produto, tipo_produto, descricao_produto, quantidade, nome_unidade "
				+ "FROM \"Produto\" INNER JOIN \"Estoque\" ON \"Estoque\".cod_produto = \"Produto\".cod_produto "
				+ "INNER JOIN \"Almoxarifado_Almoxarife\" ON \"Almoxarifado_Almoxarife\".cod_almoxarifado = \"Estoque\".cod_almox_almoxarife " 
				+ "AND \"Almoxarifado_Almoxarife\".nome_unidade NOT ILIKE 'reitoria'";
		
		PreparedStatement ps;
		ResultSet rs = null;

		try {

			ps = con.prepareStatement(query);

			rs = ps.executeQuery();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return rs;
	}

}
