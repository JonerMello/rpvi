package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.Almoxarifado;
import model.SessionContext;
import model.SolicitacaoItemProduto;



public class SolicitacaoItemProdutoDAO {
	
	private Connection con;

	public SolicitacaoItemProdutoDAO() {
		try {
			con = ConnectionFactory.getConnection();
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	public void create(SolicitacaoItemProduto solicitacao) {

		SessionContext session = new SessionContext();
		
		Almoxarifado almox = (Almoxarifado) session.getAttribute("user");

		String query = "INSERT INTO \"Pedido\" (unidade_destinataria, data_hora, quantidade, observacao, unidade_pertencente,cod_produto) "
				+ " VALUES (?, ?, ?, ?, ?, ?);";
		java.sql.Date sqlDate = new java.sql.Date(solicitacao.getDataHora().getTime());

		try {
			
			
			PreparedStatement ps = con.prepareStatement(query);
			//ps.setInt(1, 30);
			ps.setInt(1,solicitacao.getProduto().getCodigoAlmoxarifado() );
			ps.setDate(2, sqlDate);
			ps.setInt(3, solicitacao.getQuantidade());
			ps.setString(4, solicitacao.getObservacao());
			ps.setInt(5, almox.getCodigo());
			ps.setInt(6, solicitacao.getProduto().getCodigo());
		
			ps.execute();
			//con.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

	}

}
