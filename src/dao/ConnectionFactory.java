package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
	
	public static Connection getConnection() throws SQLException, ClassNotFoundException{
		try {
			Class.forName("org.postgresql.Driver");
			return DriverManager.getConnection(
												"jdbc:postgresql://localhost:5432/almoxarifado", 
												"postgres", 
												"");
		} catch (SQLException e) {
			throw new SQLException("Conex�o com banco de dados falhou.");
		} catch (ClassNotFoundException e) {
			throw new ClassNotFoundException("Driver do postgre n�o encontrado");
		}
	}
}
	

