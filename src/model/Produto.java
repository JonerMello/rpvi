package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Produto implements Serializable{

	private int codigo;
	private int codigoAlmoxarifado;
	private String nome;
	private String descricao;
	private String tipo;
	private String unidade;
	private int quantidade;

	
	public Produto(int codigo,int codigoAlmoxarifado, String nome, String descricao, String tipo, String unidade, int quantidade) {
		this.codigo = codigo;
		this.nome = nome;
		this.descricao = descricao;
		this.tipo = tipo;
		this.unidade = unidade;
		this.quantidade = quantidade;
		this.codigoAlmoxarifado = codigoAlmoxarifado;
	}

	
	public Produto() {
		
	}


	
	
	public int getCodigoAlmoxarifado() {
		return codigoAlmoxarifado;
	}


	public void setCodigoAlmoxarifado(int codigoAlmoxarifado) {
		this.codigoAlmoxarifado = codigoAlmoxarifado;
	}


	public int getCodigo() {
		return codigo;
	}


	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public String getTipo() {
		return tipo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public String getUnidade() {
		return unidade;
	}


	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}


	public int getQuantidade() {
		return quantidade;
	}


	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}	
	
}
