package model;

import java.util.ArrayList;
import java.util.List;

public class Almoxarifado {

	private int codigo;
	private String unidade;
	private String tipo;
	private String email;
	private List<Produto> estoque;
	
	public Almoxarifado(int codigo,String unidade, String tipo, String email) {
		this.codigo = codigo;
		this.unidade = unidade;
		this.tipo = tipo;
		this.email = email;
		estoque = new ArrayList<Produto>();
	}
	
	public Almoxarifado() {
		
	}

	public String getUnidade() {
		return unidade;
	}
	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<Produto> getEstoque() {
		return estoque;
	}
	public void setEstoque(List<Produto> estoque) {
		this.estoque = estoque;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	
}
