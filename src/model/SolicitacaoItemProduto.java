package model;

import java.util.Date;

public class SolicitacaoItemProduto {

	private Date dataHora;
	private int quantidade;
	private String observacao;
	private Produto produto;

	
	public SolicitacaoItemProduto(Date dataHora, int quantidade, String observacao, Produto produto) {
		
		this.dataHora = dataHora;
		this.quantidade = quantidade;
		this.observacao = observacao;
		this.produto = produto;
	}
	
	public Date getDataHora() {
		return dataHora;
	}
	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	
	

}
