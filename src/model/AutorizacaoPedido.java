package model;

import java.sql.Date;

public class AutorizacaoPedido {

	private int codigoPedido;
	private int codigoAlmoxarifado;
	private Date data;
	private String unidPertencente;
	private String unidDestinataria;
	private String nomeProduto;
	private String tipoProduto;
	private String quantidade;

	public AutorizacaoPedido(int codigoPedido, int codigoAlmoxarifado, Date data, String unidPertencente,
			String unidDestinataria, String nomeProduto, String tipoProduto, String quantidade) {
		this.codigoPedido = codigoPedido;
		this.codigoAlmoxarifado = codigoAlmoxarifado;
		this.data = data;
		this.unidPertencente = unidPertencente;
		this.unidDestinataria = unidDestinataria;
		this.nomeProduto = nomeProduto;
		this.tipoProduto = tipoProduto;
		this.quantidade = quantidade;
	}

	public int getCodigoProduto() {
		return codigoPedido;
	}

	public void setCodigoPedido(int codigoPedido) {
		this.codigoPedido = codigoPedido;
	}

	public int getCodigoAlmoxarifado() {
		return codigoAlmoxarifado;
	}

	public void setCodigoAlmoxarifado(int codigoAlmoxarifado) {
		this.codigoAlmoxarifado = codigoAlmoxarifado;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getUnidPertencente() {
		return unidPertencente;
	}

	public void setUnidPertencente(String unidPertencente) {
		this.unidPertencente = unidPertencente;
	}

	public String getUnidDestinataria() {
		return unidDestinataria;
	}

	public void setUnidDestinataria(String unidDestinataria) {
		this.unidDestinataria = unidDestinataria;
	}

	public String getNomeProduto() {
		return nomeProduto;
	}

	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}

	public String getTipoProduto() {
		return tipoProduto;
	}

	public void setTipoProduto(String tipoProduto) {
		this.tipoProduto = tipoProduto;
	}

	public String getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(String quantidade) {
		this.quantidade = quantidade;
	}

}
